#Spring-Boot-2.0-Samples

欢迎购买本书《Spring Boot 2精髓：从构建小系统到架构分布式大系统》
各大书店，各大网站均有销售。

关于本书的更多说明，可以访问ibeetl.com官网

本书源码地址是：https://gitee.com/xiandafu/Spring-Boot-2.0-Samples

依据本书技术栈实现的开发平台：https://gitee.com/xiandafu/springboot-plus

![](all.png)





过去几年，微服务架构在软件开发领域逐渐深入人心，Spring Boot在经历了快速演变之后，正在成为Java微服务开发的主流成熟框架。本书对Spring Boot的特性进行全方位讲解，辅以大量详实的案例，对分布式系统开发和应用提供实战指导。书中还详细介绍了作者倾注了大量心血研发的开源软件Beetl和BeetlSQL，它们易于与Spring Boot集成，并已被众多大公司采纳使用。本书对于开发人员和架构师来说，都极具参考价值。
—《Kubernetes权威指南》作者/HPE高级顾问 龚正


和家智相识多年，他是我所认识的非职业足球运动员中球商最高的,多年来他一直深耕于技术的第一线，有很丰富的技术储备，是我非常钦佩的老大哥。
我们曾经在同一家公司的同一个项目集效力，都非常喜欢踢球，都出了书，不得不说很神奇(作者注：其实和龚正也在同一个公司，同一个项目效力）。
这本书的内容非常丰富，也都是作者多年潜心钻研的积累，这本书和我的书有一些联系，将Spring Boot进行了展开的讲解，既有广度也有深度，非常值得技术人员去学习。
—《微服务那些事儿》作者纪晓峰




Spring Boot上手简单、功能丰富、易于扩展，可谓目前业界快速开发、快速生产的利器；然而，它的版本演进非常迅速，中文文档质量也是参差不齐。本书由浅入深地讲解了Spring Boot，帮助读者系统理解Spring Boot。不仅如此，本书对2.0版本带来的新特性亦有非常详尽的描述，绝对值得一读。

—《Spring Cloud与Docker微服务架构实战》作者 周立

和家智相识有五年了，最早是因为模板引擎技术结识。家智是国内顶级的模板引擎专家，也是我认识的为数不多的 Spring 技术专家，他在这两方面都曾带给我不同角度的思考与印证，让我获益匪浅。本书是家智二十年技术专研的一次厚积薄发，其中非但从作者自身的经验详细讲述了 Spring  Boot框架，还引入了作者在模板引擎，ORM 以及单元测试方面上多年研发的开源作品，是 Java 程序员和架构师不可多得的参考资料
— ACTFramework 作者 罗格林


十多年前，Spring 颠覆了传统的 JavaEE 技术，迎来了 Java 企业级应用开发的春天，然而今天的 Spring Boot 却站在 Spring 巨人的肩膀上，让我们可以更高效地开发与交付。李家智是著名开源框架 Beetl 的作者，他写的《Spring Boot 2.0 精髓》一定非常精彩。
— 黄勇，特赞科技 CTO


最早熟知作者是从Beetl模板引擎开始的，当时正在寻找一个易用高效的模板引擎，Beetl几乎满足了我所有的需要。同时也对作者在开源项目上的认真与负责所敬佩。本书可以说是作者多年的心血所著，从Spring boot的前世今生到使用扩展，都做了非常全面而易懂的概括，细节上也秉承了作者的细致与认真，讲解清晰并语言干练，既适合初学者系统化学习，也适合有经验的工程师做为参考。
— 开源工具集Hutool作者 路小磊(内蒙古乌海市海勃湾区滨河区公安局新楼 016000 路小磊 18847336369)



从事web开发有些年头了,经过技术选型,spring boot走入了我们的视野，开箱即用，非常方便,也是目前很多大公司的选择之一。除了研究源码，如果有一本关于SpringBoot的指导书籍，则可以极大方便的解决开发中的问题和帮助掌握SpringBoot,提高生产效率。
家智兄的这本书正是这样不可多得的优秀资源，是家智兄多年钻研研究的技术积累,书中详细讲解了web开发的各个知识点，包含:web请求处理,ORM处理,redis缓存,MongoDB，ElasticSearch,Zookeeper,监控等方面的知识点。相信读者通过仔细阅读并掌握本书的知识点，可以极大的提高自身的web开发水平，为读者的软件开发事业助一臂之力！

—  上海秦苍(买单侠) 基础架构组架构师 刘志强

作者在Java EE体系内的多年实战经验使得本书的内容极具价值，书中清晰细致的讲解了快速构建Web应用系统的各个知识点，尤其是在后端模板引擎和ORM两个章节中作者以自己的两款成熟开源产品Beetl和BeetlSQL为切入点进行讲解，剖析角度十分新颖并且有启发性。
通过这本书可以学习到关于Spring Boot框架的核心技术，从而掌握快速构建分布式Web应用的必备知识。无论你是Spring Boot新手，还是已经使用过Spring Boot的开发者，相信都可以从这本书中受益。



近两年来，伴随着微服务兴起，Spring boot突然流行起来了，越来越多的公司采用这一技术，已经成为大多数Java微服务开发首选开源框架。Spring boot有非常显著的特点配置简单，易于开发，可快速部署,本书结合丰富的实例，从Spring boot的快速开发WEB应用入手，逐渐深入的到Spring boot的高级特性，最后在重点介绍分布式架构的应用，通过深入浅出的阐述，让你从单体应用到分布式，微服务都有全方位的了解，是不可多得的一本好书，当然我认为最重要的还是作者耗费心血开源项目Beetl和BeetlSQL。
—《分布式数据库架构及企业实践——基于Mycat中间件》作者，开源中间件Mycat负责人 周继锋


Spring风靡多年，Spring Boot在最近几年微服务框架浪潮下更是出尽风头，本书作者由浅入深地把Spring Boot 2.0各种特性阐述得淋漓尽致，不管你是Spring Boot新手还是老司机都值得一读。Java Web后端也好，App后台也罢，甚至独立后台应用，等等，Spring Boot都是你不可或缺的高效率工具。

移动易项目团队深深的体会就是使用了Spring Boot可以节省50%以上的代码。
— 上海亿琪软件有限公司CEO，移动易开源项目负责人，华为开发者社区专家(HDE)，褚建琪


广州番禺区富华西路35号华南大厦815 书一本

Spring风靡多年，Spring Boot在最近几年微服务框架浪潮下更是出尽风头，本书作者由浅入深地把Spring Boot 2.0各种特性阐述得淋漓尽致，不管你是Spring Boot新手还是老司机都值得一读。Java Web后端也好，App后台也罢，甚至独立后台应用，等等，Spring Boot都是你不可或缺的高效率工具。

移动易项目团队深深的体会就是使用了Spring Boot可以节省50%以上的代码。

http://item.jd.com/12214143.html 购书地址
```
这里输入代码
```
<pre>
目录：
第1章　Java EE简介
1．1　Java EE
1．1．1　Java EE架构
1．1．2　Java EE的缺点
1．2　Spring
1．2．1　Spring IoC容器和AOP
1．2．2　Spring的缺点
1．3　Spring Boot
1．4　Hello，Spring Boot
1．4．1　创建一个Maven工程
1．4．2　增加Web支持
1．4．3　Hello Spring Boot示例
1．4．4　使用热部署
1．4．5　添加REST支持
第2章　Spring Boot基础
2．1　检查Java环境与安装Java
2．2　安装和配置Maven
2．2．1　Maven介绍
2．2．2　安装Maven
2．2．3　设置Maven
2．2．4　使用IDE设置Maven
2．2．5　Maven的常用命令
2．3　Spring核心技术
2．3．1　Spring的历史
2．3．2　Spring容器介绍
2．3．3　Spring AOP介绍
第3章　MVC框架
3．1　集成MVC框架
3．1．1　引入依赖
3．1．2　Web应用目录结构
3．1．3　Java包名结构
3．2　使用Controller
3．3　URL映射到方法
3．3．1　@RequestMapping
3．3．2　URL路径匹配
3．3．3　HTTP method匹配
3．3．4　consumes和produces
3．3．5　params和header匹配
3．4　方法参数
3．4．1　PathVariable
3．4．2　Model＆ModelAndView
3．4．3　JavaBean接受HTTP参数
3．4．4　@RequsetBody接受JSON
3．4．5　MultipartFile
3．4．6　@ModelAttribute
3．4．7　@InitBinder
3．5　验证框架
3．5．1　JSR-303
3．5．2　MVC中使用@Validated
3．5．3　自定义校验
3．6　WebMvcConfigurer
3．6．1　拦截器
3．6．2　跨域访问
3．6．3　格式化
3．6．4　注册Controller
3．7　视图技术
3．7．1　使用Freemarker
3．7．2　使用Beetl
3．7．3　使用Jackson
3．7．4　Redirect和Forward
3．8　通用错误处理
3．9　@Service和@Transactional
3．9．1　声明一个Service类
3．9．2　事务管理
3．10　curl命令
第4章　视图技术
4．1　Beetl模板引擎
4．1．1　安装Beetl
4．1．2　设置定界符号和占位符
4．1．3　配置Beetl
4．1．4　groupTemplate
4．2　使用变量
4．2．1　全局变量
4．2．2　局部变量
4．2．3　共享变量
4．2．4　模板变量
4．3　表达式
4．3．1　计算表达式
4．3．2　逻辑表达式
4．4　控制语句
4．4．1　循环语句
4．4．2　条件语句
4．4．3　try catch
4．5　函数调用
4．6　格式化函数
4．7　直接调用Java
4．8　标签函数
4．9　HTML标签
4．10　安全输出
4．11　高级功能
4．11．1　配置Beetl
4．11．2　自定义函数
4．11．3　自定义格式化函数
4．11．4　自定义标签函数
4．11．5　自定义HTML标签
4．11．6　布局
4．11．7　AJAX局部渲染
4．12　脚本引擎
4．13　JSON技术
4．13．1　在Spring Boot中使用Jackson
4．13．2　自定义ObjectMapper
4．13．3　Jackson的三种使用方式
4．13．4　Jackson树遍历
4．13．5　对象绑定
4．13．6　流式操作
4．13．7　Jackson注解
4．13．8　集合的反序列化
4．14　MVC分离开发
4．14．1　集成WebSimulate
4．14．2　模拟JSON响应
4．14．3　模拟模板渲染
第5章　数据库访问
5．1　配置数据源
5．2　Spring JDBC Template
5．2．1　查询
5．2．2　修改
5．2．3　JdbcTemplate增强
5．3　BeetlSQL介绍
5．3．1　BeetlSQL功能概览
5．3．2　添加Maven依赖
5．3．3　配置BeetlSQL
5．3．4　SQLManager
5．3．5　使用SQL文件
5．3．6　Mapper
5．3．7　使用实体
5．4　SQLManager内置CRUD
5．4．1　内置的插入API
5．4．2　内置的更新（删除）API
5．4．3　内置的查询API
5．4．4　代码生成方法
5．5　使用sqlId
5．5．1　md文件命名
5．5．2　md文件构成
5．5．3　调用sqlId
5．5．4　翻页查询
5．5．5　TailBean
5．5．6　ORM查询
5．5．7　其他API
5．5．8　Mapper详解
5．6　BeetlSQL的其他功能
5．6．1　常用函数和标签
5．6．2　主键设置
5．6．3　BeetlSQL注解
5．6．4　NameConversion
5．6．5　锁
第6章　Spring Data JPA
6．1　集成Spring Data JPA
6．1．1　集成数据源
6．1．2　配置JPA支持
6．1．3　创建Entity
6．1．4　简化Entity
6．2　Repository
6．2．1　CrudRepository
6．2．2　PagingAndSortingRepository
6．2．3　JpaRepository
6．2．4　持久化Entity
6．2．5　Sort
6．2．6　Pageable和Page
6．2．7　基于方法名字查询
6．2．8　@Query查询
6．2．9　使用JPA Query
6．2．10　Example查询
第7章　Spring Boot配置
7．1　配置Spring Boot
7．1．1　服务器配置
7．1．2　使用其他Web服务器
7．1．3　配置启动信息
7．1．4　配置浏览器显示ico
7．2　日志配置
7．3　读取应用配置
7．3．1　Environment
7．3．2　@Value
7．3．3　@ConfigurationProperties
7．4　Spring Boot自动装配
7．4．1　@Configuration和@Bean
7．4．2　Bean条件装配
7．4．3　Class条件装配
7．4．4　Environment装配
7．4．5　其他条件装配
7．4．6　联合多个条件
7．4．7　Condition接口
7．4．8　制作Starter
第8章　部署Spring Boot应用
8．1　以jar文件运行
8．2　以war方式部署
8．3　多环境部署
8．4　@Profile注解
第9章　Testing单元测试
9．1　JUnit介绍
9．1．1　JUnit的相关概念
9．1．2　JUnit测试
9．1．3　Assert
9．1．4　Suite
9．2　Spring Boot单元测试
9．2．1　测试范围依赖
9．2．2　Spring Boot测试脚手架
9．2．3　测试Service
9．2．4　测试MVC
9．2．5　完成MVC请求模拟
9．2．6　比较MVC的返回结果
9．2．7　JSON比较
9．3　Mockito
9．3．1　模拟对象
9．3．2　模拟方法参数
9．3．3　模拟方法返回值
9．4　面向数据库应用的单元测试
9．4．1　@Sql
9．4．2　XLSUnit
9．4．3　XLSUnit的基本用法
第10章　REST
10．1　REST简介
10．1．1　REST风格的架构
10．1．2　使用“api”作为上下文
10．1．3　增加一个版本标识
10．1．4　标识资源
10．1．5　确定HTTP Method
10．1．6　确定HTTP Status
10．1．7　REST VS． WebService
10．2　Spring Boot集成REST
10．2．1　集成REST
10．2．2　@RestController
10．2．3　REST Client
10．3　Swagger UI
10．3．1　集成Swagger
10．3．2　Swagger规范
10．3．3　接口描述
10．3．4　查询参数描述
10．3．5　URI中的参数
10．3．6　HTTP头参数
10．3．7　表单参数
10．3．8　文件上传参数
10．3．9　整个请求体作为参数
10．4　模拟REST服务
第11章　MongoDB
11．1　安装MongoDB
11．2　使用shell
11．2．1　指定数据库
11．2．2　插入文档
11．2．3　查询文档
11．2．4　更新操作
11．2．5　删除操作
11．3　Spring Boot集成MongoDB
11．4　增删改查
11．4．1　增加API
11．4．2　根据主键查询API
11．4．3　查询API
11．4．4　修改API
11．4．5　删除API
11．4．6　使用MongoDatabase
11．4．7　打印日志
第12章　Redis
12．1　安装Redis
12．2　使用redis-cli
12．2．1　安全设置
12．2．2　基本操作
12．2．3　keys
12．2．4　Redis List
12．2．5　Redis Hash
12．2．6　Set
12．2．7　Pub/Sub
12．3　Spring Boot集成Redis
12．4　使用StringRedisTemplate
12．4．1　opsFor
12．4．2　绑定Key的操作
12．4．3　RedisConnection
12．4．4　Pub/Sub
12．5　序列化策略
12．5．1　默认序列化策略
12．5．2　自定义序列化策略
第13章　Elasticsearch
13．1　Elasticsearch介绍
13．1．1　安装Elasticsearch
13．1．2　Elasticsearch的基本概念
13．2　使用REST访问Elasticsearch
13．2．1　添加文档
13．2．2　根据主键查询
13．2．3　根据主键更新
13．2．4　根据主键删除
13．2．5　搜索文档
13．2．6　联合多个索引搜索
13．3　使用RestTemplate访问ES
13．3．1　创建Book
13．3．2　使用RestTemplate获取搜索结果
13．4　Spring Data Elastic
13．4．1　安装Spring Data
13．4．2　编写Entity
13．4．3　编写Dao
13．4．4　编写Controller
第14章　Cache
14．1　关于Cache
14．1．1　Cache的组件和概念
14．1．2　Cache的单体应用
14．1．3　使用专有的Cache服务器
14．1．4　使用一二级缓存服务器
14．2　Spring Boot Cache
14．3　注释驱动缓存
14．3．1　@Cacheable
14．3．2　Key生成器
14．3．3　@CachePut
14．3．4　@CacheEvict
14．3．5　@Caching
14．3．6　@CacheConfig
14．4　使用Redis Cache
14．4．1　集成Redis缓存
14．4．2　禁止缓存
14．4．3　定制缓存
14．5　Redis缓存原理
14．6　实现Redis两级缓存
14．6．1　实现TwoLevelCacheManager
14．6．2　创建RedisAndLocalCache
14．6．3　缓存同步说明
14．6．4　将代码组合在一起
第15章　Spring Session
15．1　水平扩展实现
15．2　Nginx的安装和配置
15．2．1　安装Nginx
15．2．2　配置Nginx
15．3　Spring Session
15．3．1　Spring Session介绍
15．3．2　使用Redis
15．3．3　Nginx+Redis
第16章　Spring Boot和ZooKeeper
16．1　ZooKeeper
16．1．1　ZooKeeper的数据结构
16．1．2　安装ZooKeeper
16．1．3　ZooKeeper的基本命令
16．1．4　领导选取演示
16．1．5　分布式锁演示
16．1．6　服务注册演示
16．2　Spring Boot集成ZooKeeper
16．2．1　集成Curator
16．2．2　Curator API
16．3　实现分布式锁
16．4　服务注册
16．4．1　通过ServiceDiscovery注册服务
16．4．2　获取服务
16．5　领导选取
第17章　监控Spring Boot应用
17．1　安装Acutator
17．2　HTTP跟踪
17．3　日志查看
17．4　线程栈信息
17．5　内存信息
17．6　查看URL映射
17．7　查看Spring容器管理的Bean
17．8　其他监控
17．9　编写自己的监控信息
17．9．1　编写HealthIndicator
17．9．2　自定义监控


前言


Java的各种开发框架发展了很多年，影响了一代又一代的程序员，现在无论是程序员，还是架构师，使用这些开发框架都面临着两方面的挑战。
一方面是要快速开发出系统，这就要求使用的开发框架尽量简单，无论是新手还是老手都能快速上手，快速掌握页面渲染、数据库访问等常用技术。也要求开发框架能尽量多地集成第三方工具，以便信手拈来。最后，还希望在开发调试过程中，方便代码更改后能快速重启。
另外一方面，当系统模块增加，用户使用量增加时，面对这样的挑战，系统拆分成为新的架构，程序员和架构师当然不希望换掉已有的开发框架，希望能由小而美的系统过渡到大而强的分布式系统。
环顾当前Java开源世界中的流行技术框架，能同时胜任这项工作的微乎其微，Play和ActFramework 都是不错的选择，国内的Nutz和JFinal的口碑也不错。但能同时满足快速开发和分布式系统架构的框架，还是群众基础最好、功能最全、基于Spring技术的Spring Boot框架。
内容介绍
本书系统介绍了Spring Boot 2.0的主要技术，侧重于两个方面，一方面是极速开发一个Web应用系统（第1～6章，包含Spring介绍、MVC、视图技术、数据库访问技术），随后介绍了Spring Boot的高级特性（第7～9章），包括多环境部署、自动装配、单元测试等技术。另外一方面，当系统模块增加，性能和吞吐量要求增加时，如何平滑地用Spring Boot来实现分布式架构，会在本书的第10～17章介绍。
阅读本书的读者，可以是Java新手，从未使用过任何Spring技术。也可以是用过Spring，但想进一步了解Spring Boot的开发者。如果你已经使用过Spring Boot，那么本书也非常适合你全面深入了解Spring Boot。
希望读者阅读完本书后，既能轻松快速构建Web应用系统，也能掌握分布式系统架构的实现。
上半部分介绍Spring Boot的基础技术。
第1章：介绍Java EE，然后指出其缺点，引入了流行的Spring，同时也说明Spring经过这么多年发展后暴露的一些缺点，从而引出Spring Boot，并以两个简要例子作为说明。
第2章：对Spring Boot应用的开发环境做了说明，包括Java开发环境的安装和配置，Maven的安装和配置，设置国内仓库镜像，还有常用的Maven命令。本章最后介绍Spring历史以及现有开发团队，并介绍Spring框架的AOP和IoC两个核心技术
第3章：介绍MVC技术，前半部分重点介绍URL映射到Controller，以及映射到Controller 方法的参数、参数类型转化、参数验证。后半部分简单介绍MVC中的视图技术Freemaker、Beetl，以及Jackson序列化技术。Beetl和Jackson将在第4章详细介绍。
第4章：介绍笔者的开源技术Beetl后端模板引擎，作为国内流行的模板引擎之一，具有简单易学、功能/性能强大、支持MVC分离开发等特点。另外一部分详细介绍Jackson的JSON序列化技术。Jackson不仅作为Spring MVC中的JSON默认工具，也是Spring Boot分布式技术中常采用的JSON序列化技术。
第5章：介绍以SQL为中心的数据库访问工具BeetlSQL，这是笔者的另外一款流行Dao工具，SQL在markdown文件中管理，内置增删改查、轻量级ORM功能、代码生成、主从支持、跨多种数据库等特点，适合那些更喜欢以SQL方式访问数据库的开发者。
第6章：介绍以面向对象为中心的数据库访问工具Spring Data JPA。本章由易到难，先从Spring Data提供的功能入手，介绍如何完成数据库简单的增删改查功能，然后引入JPA来解决应用中不可避免的复杂SQL查询。
第7章：介绍Spring Boot高级特性，如常用的Spring Boot的配置、日志配置、应用配置的读取、Spring Boot自动装配技术和Spring Boot Starter实现。
第8章：介绍如何部署Spring Boot应用，包括可执行jar，以及通过war部署到应用服务器上。应用经常面对多个环境，如开发、测试，还有准线上、线上，以及多个Demo环境，Spring Boot提供Profile来实现多环境部署。
第9章：介绍单元测试概念，以及Spring Boot下的单元测试支持，包括MVC单元测试、Mock测试，以及面向数据库应用的测试方案。

下半部分介绍与Spring Boot相关的分布式技术。
第10章：介绍RESTful风格的架构，然后介绍Spring Boot如何集成以提供REST服务，使用RestTemplate调用REST服务。本章最后也重点介绍了Swagger 3.0技术，以方便REST的接口的交流、开发和测试。
第11章：介绍MongoDB的安装和使用，然后介绍Spring Boot如何集成MongoDB，同时还介绍了如何用MongoTemplate访问MongoDB。
第12章：介绍Redis服务器的安装和使用，Redis常用的数据结构和操作命令。然后介绍Spring Boot如何集成Redis，如何使用RedisTemplate来操作Redis。本章后半部分深入介绍了RedisTemplate提供的序列化机制。
第13章：介绍Elasticsearch的安装和使用，Elasticsearch既具有全文搜索功能，也能像MongoDB那样，具备NoSQL的功能。本章介绍通过REST和Spring Data两种方式访问Elasticsearch。
第14章：介绍Spring Boot Cache，并重点介绍Redis作为分布式缓存的实现。在此基础上，改进了Redis分布式缓存，通过较少的代码实现了一个具备一二级缓存的技术方案。
第15章：Spring Boot应用水平扩展，需实现无会话状态技术，Spring Session提供了分布式会话管理，本章介绍了Nginx作为反向代理的内容，以及Spring Session的Redis实现及其源码分析。
第16章：基于第15章Spring Boot应用水平扩展技术必然带来分布式协调要求，ZooKeeper是一个广泛使用的分布式协调器。本章介绍ZooKeeper的安装和使用，对领导选取、分布式锁和服务注册三个常用功能做了重点描述，并在Spring Boot 应用中采用Curator来完成这三个功能。
第17章：Spring Boot提供了内置监控功能，使得用户通过HTTP请求就能知晓服务器的健康状态，如数据源是否可用、NoSQL服务是否可用、最近的HTTP访问的内容等监控信息。本章讲述了线程栈、内存、在线日志、HTTP访问、RequestMapping等常用监控功能。其中还讲述通过dump线程栈和内存来解决Spring Boot应用中的一些性能故障。
如何阅读本书
笔者作为一个从事Java开发17年的程序员，这里给新手一些诚恳的建议，用于帮助新手掌握Spring Boot 2.0。
建议按照本书每章的例子先模仿一遍，不要急于按照自己的项目要求去改，否则很容易知识掌握不牢固、不全面。如果遇到自己暂时无法理解的知识，也建议优先记住这些知识点。
理解完书中的知识，能运行书中提供的例子（推荐手写，或者从官网下载例子）后，可以尝试主动制造一些错误。看看Spring Boot会给你什么样的错误提示，以1.4.5节例子为例，如果去掉@RequestMapping注解，或者如果写成value="/usercredit/{id123}" 会怎么样，甚至将getCreditLevel改成getCreditLevelTest会有什么后果（这个改动没有任何影响）。通过主动制造错误，观察Spring Boot应用的错误信息来深入学习Spring Boot。实际上，不仅仅是学习Spring Boot，这也是学习其他框架，甚至是编程语言或者其他任何编程技术的一种窍门。
本书每章都会提及Spring Boot框架的一些接口或者关键类，不了解这些类的实现细节，你仍然可以运用Spring Boot，如果想深入掌握Spring Boot，建议阅读这些类的源代码以了解这些类的职责以及如何实现职责。可以通过IDE的快捷键打开这些类，以Eclipse 为例，用快捷键Ctrl+T打开这些类去阅读Spring源码，还可以在这些类的方法中打上断点，在运行本书例子的时候，查看在断点处发生了什么来帮助你理解Spring Boot。比如在第14章中使用Redis实现分布式缓存的时候，提到了RedisCacheManager，你可以阅读这个类的源码，并在关键的getCache方法上打上断点，观察如何实现Redis缓存。
如果对于这些类还是无法理解，则可以通过搜索引擎搜索这些类，总有些博客和技术文章在讨论这些类的职责和实现方式。
谨慎对待互联网搜索结果，这是因为Spring Boot 2.0技术本身较为新，发展也较快，通过互联网搜索结果需要关注一下文章的发布日期、文章适用的版本，如果你在使用Spring Boot 2.0中遇到任何问题，也欢迎到社区进行交流，社区地址是ibeetl.com。
最后，请购买正版书籍，鼓励作者和出版社出版更好的技术书籍。
致谢
首先感谢我的妻子苗珺对我写书的大力支持，没有她的支持，我是不可能完成一本书的写作的。还有我的儿子，知道我正在做一件很重要的事情的时候就不再让我陪他聊天。
还要感谢电子工业出版社的编辑同志，给予我绝对的信任和支持，并对本书的出版做了非常多的指导，感谢你们付出的辛勤汗水。
最后要感谢的是公司和集团领导对我写书的大力支持，特别是在我需要帮助的时候委派左丽娜同事完成部分章节的编写，没有公司领导的支持，对于中国程序员来说，写一本书几乎是不可能完成的任务。
本书是我写的第一本书，由于Spring和Spring Boot技术体系博大精深，而我技术有限，写作过程中精力也有限，难免有纰漏，敬请读者指正。

东方邦信金融科技有限公司　李家智（闲大赋）
</pre>